.. Nosqlmodel documentation master file, created by
   sphinx-quickstart on Tue Dec 17 23:00:08 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

   
Welcome to Nosqlmodel's documentation!
======================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   
nosqlmodel is a first non-relational NoSql ORM framework. Easy way to create models with a nosql backend. 

Our Motto is simple :

**Speed, Speed Speed!**

So there is unneccesarry relations, object convertions, heavy queries are out of our scope!

Currently Redis and Dynamodb supported.



.. inheritance-diagram:: nosqlmodel.connectors.dynamoconnector
   :parts: 1


.. inheritance-diagram:: nosqlmodel.connectors.redisconnector
   :parts: 1

   
   
BaseNoSqlModel
===================
.. automodule:: nosqlmodel.basenosql
   :members:
 
BaseNosqlConnector
==========================
.. automodule:: nosqlmodel.connectors.baseconnector
   :members:

DynamoConnector
==========================
.. automodule:: nosqlmodel.connectors.dynamoconnector
   :members:

RedisConnector
==========================
.. automodule:: nosqlmodel.connectors.redisconnector
   :members:


