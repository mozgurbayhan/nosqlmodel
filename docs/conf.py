# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

PROJECT_ROOT = os.path.abspath(os.path.join('..'))
sys.path.insert(0, PROJECT_ROOT)

# -- Project information -----------------------------------------------------

project = 'nosqlmodel'
copyright = '2019, Mehmet Ozgur Bayhan'
author = 'Mehmet Ozgur Bayhan'

# The full version, including alpha/beta/rc tags
release = '1.0.4'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    # 'rinoh.frontend.sphinx',
    'sphinx.ext.autodoc',
    'sphinx.ext.inheritance_diagram',
    'sphinx_autodoc_annotation',
    # 'sphinx.ext.autosummary',
    # "sphinx_pyreverse"
    # 'sphinxcontrib.fulltoc',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'nature'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# MOB
# latex_elements = {
#     # The paper size ('letterpaper' or 'a4paper').
#     'papersize': 'letterpaper',
#
#     # The font size ('10pt', '11pt' or '12pt').
#     'pointsize': '10pt',
#
#     # Additional stuff for the LaTeX preamble.
#     'preamble': '',
# }


#
# nosqlmodel
# ===================
# .. automodule:: nosqlmodel.basenosql
#    :members:
#
# Connectors baseconnector
# ==========================
# .. automodule:: nosqlmodel.connectors.baseconnector
#    :members:
#
# Connectors dynamoconnector
# ==========================
# .. automodule:: nosqlmodel.connectors.dynamoconnector
#    :members:
#
# Connectors redisconnector
# ==========================
# .. automodule:: nosqlmodel.connectors.redisconnector
#    :members:
