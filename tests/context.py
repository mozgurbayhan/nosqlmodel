# -*- coding: utf-8 -*-

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..', "nosqlmodel")))
# noinspection PyUnresolvedReferences
import nosqlmodel
# noinspection PyUnresolvedReferences
import settings

__author__ = 'ozgur'
__creation_date__ = '5.12.2019' '12:37'
