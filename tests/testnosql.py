""" Test Me!"""
import os
import unittest

from typing import Type

from nosqlmodel.basenosql import BaseNoSqlModel
from nosqlmodel.connectors import dynamoconnector
from nosqlmodel.connectors import redisconnector
from nosqlmodel.connectors.dynamoconnector import DynamoConnector
from nosqlmodel.connectors.redisconnector import RedisConnector
from settings import REDIS_PORT, REDIS_HOST, DYNAMO_AWS_ACCESS_KEY_ID, \
    DYNAMO_AWS_SECRET_ACCESS_KEY, DYNAMO_REGION_NAME

__author__ = 'ozgur'
__creation_date__ = '2.12.2019' '16:24'

redisconnector.RHOST = REDIS_HOST
redisconnector.RPORT = REDIS_PORT
dynamoconnector.D_AWS_ACCESS_KEY_ID = DYNAMO_AWS_ACCESS_KEY_ID
dynamoconnector.D_AWS_SECRET_ACCESS_KEY = DYNAMO_AWS_SECRET_ACCESS_KEY
dynamoconnector.D_REGION_NAME = DYNAMO_REGION_NAME


class CarDynamo(BaseNoSqlModel):
    """ NODOC """

    def __init__(self):
        super().__init__()
        self.plate_number = ""
        self.top_speed = 20
        self.z_to_h = 100.3

    class Meta:
        """ NODOC """
        connector = DynamoConnector("Test")
        indexkey = "plate_number"


class CarRedis(BaseNoSqlModel):
    """ NODOC """

    def __init__(self):
        super().__init__()
        self.plate_number = ""
        self.top_speed = 20
        self.z_to_h = 100.3

    class Meta:
        """ NODOC """
        connector = RedisConnector(0)
        indexkey = "plate_number"


class NosqlTests(unittest.TestCase):
    """ NODOC """

    def _createdb(self, tclass: Type[BaseNoSqlModel]):
        """ NODOC """

        # for TestCar in [CarRedis, CarDynamo]:
        tcl = tclass()
        # noinspection PyBroadException
        try:
            tcl.delete_table()
            tcl.create_table()
            tcl.flush()
            faultless = True
        except Exception:
            faultless = False

        self.assertEqual(faultless, True)
        self.assertEqual(tcl.dbsize(), 0)

    def _crud(self, tclass: Type[BaseNoSqlModel]):
        """ NODOC """
        pnum = "1 TEST 35"
        tcl = tclass()
        # tcl.flush()
        tcl.plate_number = pnum
        tcl.top_speed = 100
        tcl.save_to_cache()

        tcl = tclass()
        # tcl.flush()
        tcl.plate_number = "1 TEST 36"
        tcl.top_speed = 101
        tcl.save_to_cache()

        tcl = tclass()
        # tcl.flush()
        tcl.plate_number = "1 TEST 37"
        tcl.top_speed = 102
        tcl.save_to_cache(["car"])
        tcl = tclass()
        tcl.get_or_create_by_id("1 TEST 38")
        self.assertEqual(tcl.plate_number, "1 TEST 38")
        self.assertEqual(tcl.top_speed, 20)
        tcl.top_speed = 800
        tcl.save_to_cache(["car"])

        tcl = tclass()
        tcl.get_or_create_by_id("1 TEST 38")
        self.assertEqual(tcl.plate_number, "1 TEST 38")
        self.assertEqual(tcl.top_speed, 800)

        tcl.get_by_id("1 TEST 36")
        self.assertEqual(tcl.to_dict(),
                         {'idkey': '1 TEST 36', 'plate_number': '1 TEST 36', 'top_speed': 101.0,
                          'z_to_h': 100.3})
        tcl.add_tags_to_item(["cycle"])

        alldict = {'1 TEST 36': {'idkey': '1 TEST 36', 'top_speed': 101.0, 'z_to_h': 100.3,
                                 'plate_number': '1 TEST 36'},
                   '1 TEST 35': {'idkey': '1 TEST 35', 'top_speed': 100.0, 'z_to_h': 100.3,
                                 'plate_number': '1 TEST 35'},
                   '1 TEST 38': {'idkey': '1 TEST 38', 'top_speed': 800.0, 'z_to_h': 100.3,
                                 'plate_number': '1 TEST 38'},
                   '1 TEST 37': {'idkey': '1 TEST 37', 'top_speed': 102.0, 'z_to_h': 100.3,
                                 'plate_number': '1 TEST 37'}}
        allkeys = {'1 TEST 36', '1 TEST 35', '1 TEST 38', '1 TEST 37'}
        alltags = {'car', 'cycle'}
        carkeys = {'1 TEST 37', '1 TEST 38'}
        self.assertEqual(tcl.get_all_as_dict(), alldict)
        self.assertEqual(len(tcl.get_all_as_list()), 4)
        self.assertEqual(set(tcl.get_keys()), allkeys)
        self.assertEqual(set(tcl.get_tags()), alltags)
        self.assertEqual(set(tcl.get_tag_keys("car")), carkeys)
        self.assertEqual(len(tcl.get_by_tag("car")), 2)
        self.assertEqual(isinstance(tcl.get_by_tag("car")[0], tclass), True)

        self.assertEqual(len(tcl.get_all_as_objectlist()), 4)
        self.assertEqual(len(tcl.get_all_as_objectdict()), 4)

        jtext = tcl.export_to_json_text(compress_data=True)
        self.assertGreater(len(jtext), 2)
        tfjson = "/tmp/test.json"
        tzjson = "/tmp/test.zip"
        tcl.export_to_json_zip(tzjson, compress_data=True)
        self.assertGreater(os.stat(tzjson).st_size, 2)
        tcl.export_to_json_file(tfjson, compress_data=True)
        self.assertGreater(os.stat(tfjson).st_size, 2)

        tac = tclass()
        tac.get_by_id("1 TEST 36")
        self.assertEqual(tac.get_tag_keys("cycle"), ['1 TEST 36'])
        tac.remove_item_from_tag("cycle")
        self.assertEqual(tac.get_tag_keys("cycle"), [])
        self.assertEqual(tac.dbsize(), 4)
        tac.delete()
        self.assertEqual(tac.dbsize(), 3)

        tcl.flush()
        tcl.import_from_json_text(jtext)
        self.assertEqual(tcl.dbsize(), 4)
        tcl.flush()
        tcl.import_from_json_file(tfjson)
        self.assertEqual(tcl.dbsize(), 4)
        tcl.flush()
        tcl.import_from_json_zip(tzjson)
        self.assertEqual(tcl.dbsize(), 4)

        self.assertEqual(tcl.get_by_id("fsal"), False)
        charlist = ["a", "b", "c"]
        for char in charlist:
            tcl.Meta.connector.upsert(char, {})

        tcl.Meta.connector.remove_keys(charlist)
        for char in charlist:
            self.assertEqual(bool(tcl.Meta.connector.get(char)), False)

        # Test float&int
        tcl = tclass()
        tcl.plate_number = "fitest"
        tcl.top_speed = 20
        tcl.z_to_h = 100.3
        tcl.save_to_cache()

        tcl = tclass()
        tcl.get_by_id("fitest")
        self.assertIsInstance(tcl.top_speed, int)
        self.assertIsInstance(tcl.z_to_h, float)

    def _multi(self, tclass: Type[BaseNoSqlModel]):
        """ NODOC """
        tcl35 = tclass()
        # tcl.flush()
        tcl35.plate_number = "1 TEST 35"
        tcl35.top_speed = 100.101

        tcl36 = tclass()
        # tcl.flush()
        tcl36.plate_number = "1 TEST 36"
        tcl36.top_speed = 101.102

        tcl37 = tclass()
        # tcl.flush()
        tcl37.plate_number = "1 TEST 37"
        tcl37.top_speed = 102.103
        updict = {
            "1 TEST 35": tcl35.to_dict(),
            "1 TEST 36": tcl36.to_dict(),
            "1 TEST 37": tcl37.to_dict()
        }

        tclass.save_multi(updict)
        mdict = tclass.get_multi(["1 TEST 37", "1 TEST 35"])

        self.assertEqual(isinstance(mdict, dict), True)
        self.assertEqual(len(mdict), 2)
        self.assertListEqual(sorted(mdict["1 TEST 37"].keys()),
                             sorted(["idkey", "plate_number", "top_speed", "z_to_h"]))
        for key, itemdict in mdict.items():
            self.assertEqual(key, itemdict["idkey"])

    def _delete(self, tclass: Type[BaseNoSqlModel]):
        """ NODOC """
        tac = tclass()
        tac.flush()
        self.assertEqual(tac.dbsize(), 0)
        self.assertEqual(len(tac.get_tags()), 0)

    def test_a_createdb_dynamo(self):
        """ NODOC """
        self._createdb(CarDynamo)

    def test_a_createdb_redis(self):
        """ NODOC """
        self._createdb(CarRedis)

    def test_b_crud_dynamo(self):
        """ NODOC """
        self._crud(CarDynamo)

    def test_b_crud_redis(self):
        """ NODOC """
        self._crud(CarRedis)

    def test_c_multi_dynamo(self):
        """ NODOC """
        self._multi(CarDynamo)

    def test_c_multi_redis(self):
        """ NODOC """
        self._multi(CarRedis)

    def test_d_rouge_lines(self):
        """ NODOC """
        exc = None
        try:
            # noinspection PyTypeChecker
            CarRedis.Meta.connector.get(2)
        except Exception as ex:
            exc = ex
        self.assertIsInstance(exc, TypeError)
        self.assertRaises(TypeError, )
        # noinspection PyTypeChecker
        CarRedis.Meta.connector.conn.set("2", 7)
        self.assertDictEqual(CarRedis.Meta.connector.get("2"), {"2": 7})
        CarRedis.Meta.connector.remove("2")

    def test_e_delete_dynamo(self):
        """ NODOC """
        self._delete(CarDynamo)

    def test_e_delete_redis(self):
        """ NODOC """
        self._delete(CarRedis)


if __name__ == '__main__':
    unittest.main()
