init:
	pip install -r requirements.txt

tests:
	coverage run --source nosqlmodel -m pytest tests/testnosql.py -W once::DeprecationWarning
	coverage report --show-missing
	coverage report --show-missing > .coverage_report
	pylint -E nosqlmodel

lint:
	pylint nosqlmodel

release:
	rm -rf dist
	rm -rf nosqlmodel.egg-info
	python3 setup.py sdist
	twine check dist/*
	twine upload dist/*

.PHONY: init tests lint release